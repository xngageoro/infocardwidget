<?php


namespace Xngage\Bundle\InfoCardWidgetBundle\Migrations\Schema\v1_0;


use Doctrine\DBAL\Schema\Schema;
use Oro\Bundle\AttachmentBundle\Migration\Extension\AttachmentExtensionAwareInterface;
use Oro\Bundle\AttachmentBundle\Migration\Extension\AttachmentExtensionAwareTrait;
use Oro\Bundle\MigrationBundle\Migration\Migration;
use Oro\Bundle\MigrationBundle\Migration\QueryBag;

class XngageInfoCardWidgetBundle implements Migration, AttachmentExtensionAwareInterface
{
    use AttachmentExtensionAwareTrait;
    const MAX_IMAGE_SIZE_IN_MB = 10;

    public function up(Schema $schema, QueryBag $queries)
    {
        $this->createContentWidgetImageTable($schema);
    }

    public function createContentWidgetImageTable(Schema $schema) {
        if(!$schema->hasTable('xng_cms_widget_image')) {
            $table = $schema->createTable('xng_cms_widget_image');
            $table->addColumn('id', 'integer', ['autoincrement' => true]);
            $table->addColumn('content_widget_id', 'integer');


            $this->attachmentExtension->addImageRelation(
                $schema,
                'xng_cms_widget_image',
                'image',
                ['attachment' => ['acl_protected' => false, 'use_dam' => true]],
                self::MAX_IMAGE_SIZE_IN_MB
            );

            $table->setPrimaryKey(['id']);
        }
    }
}