<?php


namespace Xngage\Bundle\InfoCardWidgetBundle\Form\Type;


use Oro\Bundle\AttachmentBundle\Entity\Attachment;
use Oro\Bundle\AttachmentBundle\Entity\File;
use Oro\Bundle\AttachmentBundle\Form\Type\ImageType;
use Oro\Bundle\CMSBundle\Entity\ContentWidget;
use Oro\Bundle\CMSBundle\Entity\ImageSlide;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Xngage\Bundle\InfoCardWidgetBundle\Entity\ContentWidgetImage;

class ContentWidgetImageType extends AbstractType
{
    const NAME = 'content_widget_image';

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'image',
            ImageType::class,
            [
                'label' => '',
                'required' => false,
                'checkEmptyFile' => false,
                'allowDelete' => true,
            ]
        );
        $builder->addEventListener(FormEvents::SUBMIT, [$this, 'setContentWidget']);

    }
    public  function setContentWidget(FormEvent $event) {
        $data = $event->getData();
        if($data instanceof ContentWidgetImage) {
            dump($event->getForm()->getConfig()->getOption('content_widget'));
            $data->setContentWidget($event->getForm()->getConfig()->getOption('content_widget'));

        }
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => ContentWidgetImage::class]);
        $resolver->setRequired(['content_widget']);
        $resolver->setAllowedTypes('content_widget', [ContentWidget::class]);
    }

    public function getBlockPrefix()
    {
        return self::NAME;
    }
}