<?php


namespace Xngage\Bundle\InfoCardWidgetBundle\EventListener;


use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManager;
use Oro\Bundle\CMSBundle\ContentWidget\ImageSliderContentWidgetType;
use Oro\Bundle\CMSBundle\Entity\ContentWidget;
use Oro\Bundle\CMSBundle\Entity\ImageSlide;
use Oro\Bundle\FormBundle\Event\FormHandler\AfterFormProcessEvent;
use Xngage\Bundle\InfoCardWidgetBundle\ContentWidget\InfoCardWidgetType;
use Xngage\Bundle\InfoCardWidgetBundle\Entity\ContentWidgetImage;

class ContentWidgetFormEventListener
{
    /** @var EntityManager */
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param AfterFormProcessEvent $args
     */
    public function onBeforeFlush(AfterFormProcessEvent $args): void
    {
        $contentWidget = $args->getData();
        if (!$contentWidget instanceof ContentWidget ||
            $contentWidget->getWidgetType() !== InfoCardWidgetType::getName()
        ) {
            return;
        }

        $settings = $contentWidget->getSettings();

        /** @var ContentWidgetImage $newImage */
        $newImage = $settings['image'] ?? null;

        if($newImage !== null) {
            $this->em->persist($newImage);
        }

        $oldImage = $this->em->getRepository(ContentWidgetImage::class)
            ->findOneBy(['contentWidget' => $contentWidget]);

        if($oldImage !== null &&($newImage === null || $newImage->getId() !== $oldImage->getId())) {
            $this->em->remove($oldImage);
        }

        unset($settings['imageSlides']);

        $contentWidget->setSettings($settings);
    }
}