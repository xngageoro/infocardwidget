<?php


namespace Xngage\Bundle\InfoCardWidgetBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Oro\Bundle\CMSBundle\Entity\ContentWidget;
use Oro\Bundle\EntityConfigBundle\Metadata\Annotation\Config;
use Oro\Bundle\EntityConfigBundle\Metadata\Annotation\ConfigField;
use Xngage\Bundle\InfoCardWidgetBundle\Model\ExtendContentWidgetImage;

/**
 * Holds content widget image data.
 *
 * @ORM\Entity()
 * @ORM\Table(name="xng_cms_widget_image")
 * @Config(
 *      defaultValues={
 *          "security"={
 *              "type"="ACL",
 *              "group_name"=""
 *          },
 *          "dataaudit"={
 *              "auditable"=true
 *          }
 *     }
 * )
 */
class ContentWidgetImage extends ExtendContentWidgetImage
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var ContentWidget
     *
     * @ORM\ManyToOne(targetEntity="Oro\Bundle\CMSBundle\Entity\ContentWidget")
     * @ORM\JoinColumn(name="content_widget_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * @ConfigField(
     *      defaultValues={
     *          "dataaudit"={
     *              "auditable"=true
     *          }
     *      }
     * )
     */
    protected $contentWidget;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return ContentWidget
     */
    public function getContentWidget(): ContentWidget
    {
        return $this->contentWidget;
    }

    /**
     * @param ContentWidget $contentWidget
     */
    public function setContentWidget(ContentWidget $contentWidget): void
    {
        $this->contentWidget = $contentWidget;
    }
}