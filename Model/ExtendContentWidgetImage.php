<?php


namespace Xngage\Bundle\InfoCardWidgetBundle\Model;

use Oro\Bundle\AttachmentBundle\Entity\File;
use Xngage\Bundle\InfoCardWidgetBundle\Entity\ContentWidgetImage;

/**
 * Extend class which allow to make ImageSlide entity extandable.
 *
 * @method null|File getImage()
 * @method ContentWidgetImage setImage(File $image)
 */
class ExtendContentWidgetImage
{
    /**
     * Constructor
     *
     * The real implementation of this method is auto generated.
     *
     * IMPORTANT: If the derived class has own constructor it must call parent constructor.
     */
    public function __construct()
    {
    }
}