<?php


namespace Xngage\Bundle\InfoCardWidgetBundle\ContentWidget;


use Doctrine\ORM\EntityManager;
use Doctrine\Persistence\ManagerRegistry;
use Oro\Bundle\AttachmentBundle\Entity\File;
use Oro\Bundle\AttachmentBundle\Form\Type\AttachmentType;
use Oro\Bundle\AttachmentBundle\Form\Type\FileType;
use Oro\Bundle\AttachmentBundle\Form\Type\ImageType;
use Oro\Bundle\CMSBundle\ContentWidget\AbstractContentWidgetType;
use Oro\Bundle\CMSBundle\Entity\ContentWidget;
use Oro\Bundle\FormBundle\Form\Type\OroRichTextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\ColorType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Twig\Environment;
use Xngage\Bundle\InfoCardWidgetBundle\Entity\ContentWidgetImage;
use Xngage\Bundle\InfoCardWidgetBundle\Form\Type\ContentWidgetImageType;

class InfoCardWidgetType extends AbstractContentWidgetType
{
    /** @var EntityManager  */
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }
    public static function getName(): string
    {
       return "info_card";
    }
    public function getLabel(): string
    {
        return 'xngage.widgets.info_card.label';
    }

    public function getSettingsForm(ContentWidget $contentWidget, FormFactoryInterface $formFactory): ?FormInterface
    {

        $builder =  $formFactory->createBuilder(FormType::class)
            ->add('title', TextType::class, [
                'required' => true,
                'label' => 'xngage.widgets.info_card.title.label',
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('image', ContentWidgetImageType::class, [
                'required' => false,
                'content_widget' => $contentWidget,
                'data' => $this->getImage($contentWidget)
            ])
            ->add('description' , OroRichTextType::class, [
                'required' => false,
                'label' => 'xngage.widgets.info_card.desc.label'
            ])
            ->add('ctaType', ChoiceType::class, [
                'label' => 'xngage.widgets.info_card.cta_type.label',
                'choices' => ['button' => 0, 'link' => 1]
            ])
            ->add('ctaText', TextType::class, [
                'required' => false,
                'label' => 'xngage.widgets.info_card.cta_text.label',
            ])
            ->add('ctaLink', TextType::class, [
                'required' => true,
                'label' => 'xngage.widgets.info_card.cta_url.label',
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('background', ColorType::class, [
                'required' => false,
                'label' => 'xngage.widgets.info_card.background.label'
            ]);



        return $builder->getForm();

    }
    public function getWidgetData(ContentWidget $contentWidget): array
    {
        $image = $this->getImage($contentWidget);
        return [
            'card' => ["data" => array_merge($contentWidget->getSettings(), ['image' => $image !== null ? $image->getImage() : null])],
            'layout' => ["data" => $contentWidget->getLayout()]
        ];
    }

    public function getDefaultTemplate(ContentWidget $contentWidget, Environment $twig): string
    {
        return '';
    }

    private function getImage(ContentWidget $contentWidget) {
        $image = $this->em->getRepository(ContentWidgetImage::class)->findOneBy(['contentWidget' => $contentWidget]);
        return $image;
    }

}